import * as React from 'react';
import { KitsuCharacter } from '../../Models/Character';
import RestService from '../../Services/RestService';
import TableList from '../../Components/TableList';
import { CircularProgress } from '@material-ui/core';

export interface AnimeProps {}

export interface State {
  list: KitsuCharacter[];
  loading: false;
  search: string;
}

const homeStyle: React.CSSProperties = {
  padding: '30px 20px',
  textAlign: 'center',
};

export default class Anime extends React.Component<AnimeProps, any> {
  private restService: RestService = new RestService();
  public state: State;
  constructor(props: React.Props<any>) {
    super(props);
  }
  public getList = (): void => {
    this.setState({ loading: true });
    // const filters = 'page[limit]=10&page[offset]=0';
    this.setState({ loading: true });
    this.restService.getCharacters('anime', (data: KitsuCharacter[]) => {
      this.setState({ list:data, loading: false });
    });
  }
  public componentWillMount() {
    this.getList();
  }
  public render() {
    return (
      <div className="Home">
        <div style={homeStyle}>
          {this.state.loading === false && this.state.list ? (
            <TableList data={this.state.list} />
          ) : (
            <CircularProgress />
          )}
        </div>
      </div>
    );
  }
}
