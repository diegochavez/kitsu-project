import * as React from 'react';
import { KitsuCharacter } from '../../Models/Character';
import RestService from '../../Services/RestService';
import TableList from '../../Components/TableList';
import { CircularProgress } from '@material-ui/core';

export interface MangaProps {}
export interface State {
  list: KitsuCharacter[];
  loading: boolean;
}

const homeStyle: React.CSSProperties = {
  padding: '30px 20px',
  textAlign: 'center',
};

export default class Anime extends React.Component<MangaProps, any> {
  private restService: RestService = new RestService();
  public state: State = {
    list: [] as KitsuCharacter[],
    loading: false,
  };
  constructor(props: React.Props<any>) {
    super(props);
  }
  public getList = (): void => {
    this.setState({ loading: true });
    this.restService.getCharacters('manga', (data: KitsuCharacter[]) => {
      this.setState({ list:data, loading: false });
    });
  }
  public componentWillMount() {
    this.getList();
  }
  public render() {
    return (
      <div className="Home">
        <div style={homeStyle}>
          {!this.state.loading && this.state.list ? (
            <TableList data={this.state.list} />
          ) : (
            <CircularProgress />
          )}
        </div>
      </div>
    );
  }
}
