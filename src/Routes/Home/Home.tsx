import * as React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import RestService from '../../Services/RestService';
import SearchBox from '../../Components/SearchBox/SearchBox';
import CardList from '../../Components/CardList';
import Kitsu from '../../Models/Kitsu';

export interface State {
  list: Kitsu[];
  loading: boolean;
  term: string;
  type: string;
}
const homeStyle: React.CSSProperties = {
  padding: '30px 20px',
  textAlign: 'center',
};
class Home extends React.Component<any> {
  public restService = new RestService();
  public state: State = {
    list: [],
    loading: false,
    term: '',
    type: 'anime',
  };
  constructor(props: React.Props<any>) {
    super(props);
  }
  public getList = (): void => {
    this.setState({ loading: true });
    this.restService.searchKitsu(this.state.term, this.state.type, (data: Kitsu[]) => {
      this.setState({ loading: false, list: data });
    });
  }
  public componentWillMount() {
    this.getList();
  }
  doSearch = (search: any): void => {
    this.setState({ type: search.type, term: search.term }, () => {
      this.getList();
    });
  }
  public render() {
    return (
      <div className="Home">
        <SearchBox placeholder={`Search title, character`} doSearch={this.doSearch} />
        <div style={homeStyle}>
          {!this.state.loading && this.state.list ? (
            <CardList data={this.state.list} />
          ) : (
            <CircularProgress />
          )}
        </div>
      </div>
    );
  }
}

export default Home;
