import axios from 'axios';

class restService {
  public fieldsCharacters = `fields[characters]=name,createdAt,updatedAt,image`;
  public fieldsMedia = `fields[media]=createdAt,updatedAt,coverImage,canonicalTitle`;

  private service = axios.create({
    baseURL: 'https://kitsu.io/api/edge/',
    headers: {
      Accept: 'application/vnd.api+json',
      ContentType: 'application/vnd.api+json',
    },
    timeout: 5000,
  });

  public searchKitsu(term: string, type: string, done: Function): void {
    let searchTerm = '';
    if (term !== '') {
      searchTerm = `filter[text]=${term}&`;
    }
    const path = `${type}?${searchTerm}${this.fieldsMedia}`;
    this.service.get(path).then(
      (res) => {
        const { data } = res.data;
        done(data);
      },
      (error) => {
        done(error);
      },
    );
  }

  public getCharacters(type: string, done: Function) {
    const path = `${type}-characters?${this.fieldsCharacters}`;
    this.service.get(path).then(
      (res) => {
        const { data } = res.data;
        done(data);
      },
      (error) => {
        done(error);
      },
    );
  }
}

export default restService;
