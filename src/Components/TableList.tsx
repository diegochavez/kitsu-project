import * as React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { KitsuCharacter } from '../Models/Character';

const tableList: React.StatelessComponent<{ data: KitsuCharacter[] }> = (
  props,
) => {
  return (
    <Paper>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Id</TableCell>
            <TableCell>Name</TableCell>
            <TableCell>Type</TableCell>
            <TableCell>Created At</TableCell>
            <TableCell>Updated At</TableCell>
            <TableCell>Role</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.data.map((character: KitsuCharacter) => {
            return (
              <TableRow key={character.id}>
                <TableCell>{character.id}</TableCell>
                <TableCell>{character.attributes.name}</TableCell>
                <TableCell>{character.type}</TableCell>
                <TableCell>{character.attributes.createdAt}</TableCell>
                <TableCell>{character.attributes.updatedAt}</TableCell>
                <TableCell>{character.attributes.role}</TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </Paper>
  );
};

export default tableList;
