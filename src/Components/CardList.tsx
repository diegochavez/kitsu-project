import * as React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import CardMedia from '@material-ui/core/CardMedia';
import Kitsu from '../Models/Kitsu';
import Grid from '@material-ui/core/Grid';

const styles = {
  card: {
    maxWidth: 345,
  },
  media: {
    height: 100,
    paddingTop: '56.25%', // 16:9
  },
};

export interface cardProps {
  data: Kitsu[];
  classes: {
    card: any;
    media: any;
  };
}

const simpleMediaCard: React.StatelessComponent<cardProps> = (props) => {
  const { classes } = props;
  return (
    <React.Fragment>
      <Grid container spacing={24}>
        {props.data.map((kitsu: Kitsu) => {
          return (
            <Grid item md={3} xs={6} key={kitsu.id}>
              <Card className={classes.card} >
                <CardContent>
                  {kitsu.attributes.coverImage &&
                  kitsu.attributes.coverImage.original ? (
                    <CardMedia
                      className={classes.media}
                      image={kitsu.attributes.coverImage.original}
                      title={kitsu.attributes.canonicalTitle}
                    />
                  ) : (
                    <CardMedia
                    className={classes.media}
                    image={'http://via.placeholder.com/350x350?text=No+image+available'}
                    title={kitsu.attributes.canonicalTitle}
                    />
                  )}
                  <Typography gutterBottom component="h2">
                    {kitsu.attributes.canonicalTitle}
                  </Typography>
                </CardContent>
              </Card>
            </Grid>
          );
        })}
      </Grid>
    </React.Fragment>
  );
};

export default withStyles(styles)(simpleMediaCard);
