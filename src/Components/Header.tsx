import * as React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import HistoryService from '../Services/HistoryService';

const styles = {
  root: {
    flexGrow: 1,
  },
  flex: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
    color: 'white',
  },
};

const linkStyle = {
  color: 'white',
  textDecoration: 'none',
};

const header: React.SFC<any> = (props) => {
  const { classes } = props;
  return (
    <React.Fragment>
      <AppBar position="fixed" color="primary">
        <Toolbar>
          <Typography className={classes.flex} variant="title" color="inherit">
            <Link style={linkStyle} to="/">
              Kitsu
            </Link>
          </Typography>
          <Button
            color="inherit"
            onClick={() => HistoryService.push('/')}>
            Home
          </Button>
          <Button
            color="inherit"
            onClick={() => HistoryService.push('/anime')}>
            Anime Characters
          </Button>
          <Button
            color="inherit"
            onClick={() => HistoryService.push('/manga')}>
            Manga Characters
          </Button>
        </Toolbar>
      </AppBar>
    </React.Fragment>
  );
};

export default withStyles(styles)(header);
