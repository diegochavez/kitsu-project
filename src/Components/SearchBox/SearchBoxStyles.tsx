const searchBoxStyles = {
  root: {
    padding: 20,
    marginBottom: 10,
  } as React.CSSProperties,
  box: {
    display: 'flex',
    flexDirection: 'row',
  } as React.CSSProperties,
  selectFieldHolder: {
    width: '20%',
    margin: '0 10px 0 0',
  } as React.CSSProperties,
  selectField: {
    width: '100%',
  } as React.CSSProperties,
  textFieldHolder: {
    width: '100%',
  } as React.CSSProperties,
  textField: {
    margin: '0',
  } as React.CSSProperties,
  button: {
    minWidth: '8rem',
    margin: '0 0 0 10px',
  } as React.CSSProperties,
};

export default searchBoxStyles;
