import * as React from 'react';
import { TextField, Button, Paper, Select, MenuItem } from '@material-ui/core';
import SearchBoxStyles from './SearchBoxStyles';

export interface SearchBoxProps {
  placeholder?: string;
  doSearch: Function;
}

export interface SearchBoxState {
  term: string;
  type: string;
}

class SearchBox extends React.Component<SearchBoxProps, SearchBoxState> {
  public state = {
    term: '',
    type: 'anime',
  };
  handleChange(target: HTMLInputElement): void {
    this.setState({ term: target.value });
  }
  changeType(e: HTMLSelectElement): void {
    this.setState({ type: e.value });
  }
  searchTerm = (): void => {
    this.props.doSearch(this.state);
  }
  handleKeyPress = (key: string): void => {
    if (key === 'Enter') {
      this.searchTerm();
    }
  }
  public render() {
    return (
      <Paper elevation={1} style={SearchBoxStyles.root}>
        <div className="search" style={SearchBoxStyles.box}>
          <div style={SearchBoxStyles.selectFieldHolder}>
            <Select
            style={SearchBoxStyles.selectField}
              value={this.state.type}
              onChange={e => this.changeType(e.target)}
              inputProps={{
                name: 'age',
                id: 'age-simple',
              }}
            >
              <MenuItem value="anime">Anime</MenuItem>
              <MenuItem value="manga">Manga</MenuItem>
            </Select>
          </div>
          <div style={SearchBoxStyles.textFieldHolder}>
            <TextField
              style={SearchBoxStyles.textField}
              id="name"
              placeholder={this.props.placeholder}
              value={this.state.term}
              onChange={e => this.handleChange(e.target)}
              onKeyPress={e => this.handleKeyPress(e.key)}
              margin="normal"
              fullWidth
            />
          </div>
          <div>
            <Button
              style={SearchBoxStyles.button}
              variant="contained"
              color="secondary"
              onClick={this.searchTerm}
            >
              Search
            </Button>
          </div>
        </div>
      </Paper>
    );
  }
}

export default SearchBox;
