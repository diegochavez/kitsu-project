// tslint:disable:no-console

import * as React from 'react';
import './App.css';
import { Router, Route } from 'react-router-dom';
import Header from './Components/Header';
import Home from './Routes/Home/Home';
import HistoryService from './Services/HistoryService';
import Anime from './Routes/Anime/Anime';
import Manga from './Routes/Manga/Manga';

class App extends React.Component<any> {
  public render() {
    return (
      <React.Fragment>
      <Router history={HistoryService}>
      <div className="App">
      <Header {...this.props} />
        <Route exact path="/" component={Home} />
        <Route exact path="/anime" component={Anime} />
        <Route exact path="/manga" component={Manga} />
      </div>
    </Router>
    </React.Fragment>
    );
  }
}

export default App;
