export interface Links {
  self: string;
}

export interface Attributes {
  createdAt: Date;
  updatedAt: Date;
  coverImage: {
    large?: string;
    tiny?: string;
    original: string;
    small?: string;
  };
  canonicalTitle: string;
}

export default interface Kitsu {
  id: string;
  type: string;
  links: Links;
  attributes: Attributes;
}
