export interface Links {
  self: string;
}

export interface Attributes {
  createdAt?: any;
  updatedAt?: any;
  role: string;
  name: string;
}

export interface Links2 {
  self: string;
  related: string;
}

export interface Anime {
  links: Links2;
}

export interface Links3 {
  self: string;
  related: string;
}

export interface Character {
  links: Links3;
}

export interface Links4 {
  self: string;
  related: string;
}

export interface Castings {
  links: Links4;
}

export interface Relationships {
  anime: Anime;
  character: Character;
  castings: Castings;
}

export interface KitsuCharacter {
  id: string;
  type: string;
  links: Links;
  attributes: Attributes;
  relationships: Relationships;
}
