# Installation
## requirements 
- nodejs & npm latest version
- clone the project and run `npm i`
- run development version `npm run start`
- run build version `npm run build`

# Tools used
- React - Create React App
- Axios
- TypeScript
- Material UI - React
- React Router

# Kitsu Project

Check the project online 
https://condescending-babbage-421683.netlify.com/

Simple anime/manga application built on top of reactjs.

## KIT-001 As a user I would like to see an alphabetically sorted list of Anime Characters 

         I would like to open a browser and see a list of Anime Characters sorted alphabetically. The list must contain the ID, Name, Type, CreatedAt, UpdatedAt, and Role

## KIT-002 As a user I would like to see an alphabetically sorted list of Manga Characters 

         I would like to open a browser and see a list of Manga Characters sorted alphabetically. The list must contain the ID, Name, Type, CreatedAt, UpdatedAt, and Role

## KIT-003 As a user I would like to search a name on the list of Manga Characters 

       I would like to have a search box where I can type the Name of a character and update the list with the data of that single Manga Characters

## KIT-004 As a user I would like to search a name on the list of Anime Characters
    
       I would like to have a search box where I can type the Name of a character and update the list with the data of that single Anime Character
